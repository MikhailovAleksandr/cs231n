
### Q1: Training a Support Vector Machine
The notebook svm.ipynb will walk you through implementing the SVM classifier.

### Q2: Implement a Softmax classifier
The notebook softmax.ipynb will walk you through implementing the Softmax classifier.

### Q3: Two-Layer Neural Network
The notebook two_layer_net.ipynb will walk you through the implementation of a two-layer neural network classifier.

### Q4: Higher Level Representations: Image Features
The notebook features.ipynb will examine the improvements gained by using higher-level representations as opposed to using raw pixel values.

### Extra: k-Nearest Neighbor classifier
The notebook knn.ipynb will walk you through implementing the kNN classifier.